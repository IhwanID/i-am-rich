import 'package:flutter/material.dart';

void main() {
  String url = 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/74/Kotlin-logo.svg/1200px-Kotlin-logo.svg.png';
  runApp(
    MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.white30,
        appBar: AppBar(
          title: Text('I am Rich'),
          backgroundColor: Colors.red,
        ),
        body: Center(child: Image(image: AssetImage('images/diamond.png'),),),
      ),
    ),
  );

}